
import json
import pytest
from mock import Mock

from wally_client import WallyClient


@pytest.fixture(scope='function')
def requests_mock(monkeypatch):
    response = Mock(status_code=200)
    mock = Mock(
        get=Mock(return_value=response)
    )
    monkeypatch.setattr('wally_client.client.requests', mock)
    return mock


@pytest.fixture(scope='function')
def start_new_thread_mock(monkeypatch):
    mock = Mock()
    monkeypatch.setattr('wally_client.client.start_new_thread', mock)
    return mock


@pytest.fixture(scope='function')
def client():
    client = WallyClient(host='http://host', key='123456789')
    return client


EXPECTED_HEADERS = {
    'content-type': 'application/json; charset=utf8',
    'Authorization': '123456789',
}


def test_get_profile(client, requests_mock):
    resp = client.get_profile('email@email.em')
    requests_mock.get.assert_called_once_with(
        url='http://host/profiles/email@email.em/',
        headers=EXPECTED_HEADERS,
        data=None,
    )
    assert resp == requests_mock.get.return_value.json.return_value


def test_update_profile(client, requests_mock):
    client.update_profile('email@email.em', 'Username', {'v1': 'k1'})
    requests_mock.put.assert_called_once_with(
        url='http://host/profiles/email@email.em/',
        data=json.dumps({
            'name': 'Username',
            'data': {'v1': 'k1'},
        }),
        headers=EXPECTED_HEADERS
    )


def test_create_action(client, requests_mock):
    client.create_action('email@email.em', {'v1': 'k1'})
    requests_mock.post.assert_called_once_with(
        url='http://host/profiles/email@email.em/actions/',
        data=json.dumps({
            'data': {'v1': 'k1'},
        }),
        headers=EXPECTED_HEADERS
    )


def test_async_call(client, requests_mock, start_new_thread_mock):
    client.sync = False

    client.request(
        'post',
        url='url1',
        data={'a': 'b'},
    )

    start_new_thread_mock.assert_called_once_with(
        requests_mock.post,
        (),
        {
            'url': 'url1',
            'headers': EXPECTED_HEADERS,
            'data': json.dumps({'a': 'b'})
        }
    )
