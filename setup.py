#!/usr/bin/env python

from distutils.core import setup

setup(name='WallyClient',
      version='0.0.1',
      description='WallyClient',
      author='Vitaly Pikulik',
      author_email='v.pikulik@gmail.com',
      url='https://bitbucket.org/vpikulik/wally_client',
      packages=['wally_client'],
      install_requires=['mock', 'requests==2.4.1', 'pytest'],
     )