
import json
import requests
import logging
from urllib import basejoin
from thread import start_new_thread

logger = logging.getLogger(__name__)


class WallyClient(object):

    def __init__(self, host, key, sync=True):
        self.host = host
        self.key = key
        self.sync = sync

    class Error(Exception):
        pass

    def request(self, method, url, data=None):
        try:
            func = getattr(requests, method)
        except AttributeError:
            raise self.Error()

        attrs = {
            'url': url,
            'data': json.dumps(data) if data else None,
            'headers': self.get_headers(),
        }

        if self.sync:
            return func(**attrs)
        else:
            start_new_thread(func, (), attrs)
            return True

    def get_headers(self):
        return {
            'content-type': 'application/json; charset=utf8',
            'Authorization': self.key,
        }

    def get_url(self, resource):
        return basejoin(self.host, resource)

    def get_profile(self, email):
        logger.info('action="wally_get_profile", email="%s"', email)
        resp = self.request(
            'get',
            self.get_url('/profiles/{0}/'.format(email)),
        )
        if resp.status_code != 200:
            raise self.Error()

        return resp.json()

    def update_profile(self, email, name, data):
        logger.info('action="wally_update_profile", email="%s", data="%s"',
                    email, data)
        return self.request(
            'put',
            self.get_url('/profiles/{0}/'.format(email)),
            data={
                'name': name,
                'data': data,
            }
        )

    def create_action(self, email, data):
        logger.info('action="wally_create_action", email="%s", data="%s"',
                    email, data)
        return self.request(
            'post',
            self.get_url('/profiles/{0}/actions/'.format(email)),
            data={
                'data': data,
            }
        )
