
# Build status

[![Build Status](https://drone.io/bitbucket.org/vpikulik/wally_client/status.png)](https://drone.io/bitbucket.org/vpikulik/wally_client/latest)

# Installation

```bash
pip install git+https://vpikulik@bitbucket.org/vpikulik/wally_client.git
```

# Initialization
```python
from wally_client import WallyClient
client = WallyClient(host='http://host.com/', key='123')
```

# Create/Update profile
```python
client.update_profile(email='user@email.em', name='Example user', data={'order_count': 12})
```

# Get profile
```python
client.get_profile('user@email.em')
```

## Response:
    {u'data': {u'order_count': 12},
     u'email': u'user@email.em',
     u'name': u'Example user'}

# Async call
```python
client = WallyClient(host='http://host.com/', key='123', sync=False)
client.update_profile(email='user@email.em', name='Example user', data={'order_count': 12})
```